# SIASApp

SIASAR Mobile Application built with [Cordova](http://cordova.apache.org/) and  [PhoneGap](http://phonegap.com).

[![build status](https://gitlab.com/Admin_Siasar/SIASAR-Movil/badges/master/build.svg)](https://gitlab.com/Admin_Siasar/SIASAR-Movil/commits/master)

### How to run it locally

1. Install [node](http://nodejs.org)

1. Install global dependencies

        npm install -g phonegap handlebars

1. Install development dependencies

        npm install

1. Add Android platform

        phonegap platform add android

1. Compile handlebars templates

        handlebars -m -e hbs www/tpl/* -f www/js/templates.js (UNIX flavours)
        handlebars -m -e hbs www\tpl\* -f www\js\templates.js (Windows)

1. Build application

        phonegap build android

1. Run application

        phonegap run android
