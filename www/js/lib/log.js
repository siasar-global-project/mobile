var Log = function(container) {
  this.container = container || $('<div>');
  this.clear();
};

Log.prototype.add = function(string) {
  var id = Math.round(Date.now() * Math.random());
  this.container.append($('<p>').text(string).prop('id', id));
  return id;
};

Log.prototype.remove = function(id) {
  var element = this.container.find('#' + id);
  setTimeout(function() {
    element.fadeOut(1000);
  }, 3000);
};

Log.prototype.clear = function() {
  this.container.empty();
};
