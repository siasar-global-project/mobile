/* global Store */

var User = function(data) {
  if (data) {
    this.id = data.id;
    this.username = data.username;
    this.email = data.email;
    this.language = data.language;
    this.name = data.name || data.username;
    this.password = data.password;
    this.country = data.country;
    this.autosave = data.autosave;
    this.server = data.server;
    this.admin = data.admin;
  }
};

User.defaultLanguage = window.navigator.userLanguage || window.navigator.language || 'es';

User.get = function() {
  var user = Store.getInstance().getUser();
  if (user) {
    user = new User(user);
  }
  return user;
};

User.prototype.save = function() {
  Store.getInstance().saveUser(this);
};

User.setLanguage = function(language) {
  var user = User.get();
  if (user) {
    user.language = language;
    user.save();
  }
};

User.getLanguage = function() {
  return new Promise(function(resolve) {
    var user = User.get();
    if (user && user.language) {
      resolve(user.language);
    } else {
      navigator.globalization.getPreferredLanguage(
        function(language) {
          resolve(language.value);
        },
        function() {
          resolve(User.defaultLanguage);
        });
    }
  });
};
